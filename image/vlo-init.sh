#!/bin/sh
set -e

filter_file() {
	TARGET_FILE=$1
	shift
	if [ -e "$TARGET_FILE" ]; then
		/bin/bash /opt/filter-config-file.sh  $TARGET_FILE $@
	else
		echo "ERROR: file not found, cannot filter: $TARGET_FILE"
		exit 1
	fi
}

# Filter VLO configuration
filter_file ${VLO_DOCKER_CONFIG_FILE} \
	VLO_DOCKER_EXPOSURE_DB_NAME \
	VLO_DOCKER_EXPOSURE_DB_HOST \
	VLO_DOCKER_EXPOSURE_DB_PORT \
	VLO_DOCKER_EXPOSURE_DB_USER \
	VLO_DOCKER_EXPOSURE_DB_PASSWORD

filter_file ${CATALINA_BASE}/conf/Catalina/localhost/exposure.xml \
	VLO_DOCKER_CONFIG_FILE \
	VLO_DOCKER_WICKET_CONFIGURATION

# Tomcat env
filter_file ${CATALINA_BASE}/bin/setenv.sh \
	VLO_DOCKER_TOMCAT_JAVA_OPTS
