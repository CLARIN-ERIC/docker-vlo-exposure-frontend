VLO_VERSION="4.8.0"

##SNAPSHOT
#SNAPSHOT_ID="4.8-20191214.143843-4"
#REPOSITORY="clarin-snapshot"

##STABLE
SNAPSHOT_ID="${VLO_VERSION}"
REPOSITORY="Clarin"

REMOTE_RELEASE_URL="https://nexus.clarin.eu/repository/${REPOSITORY}/eu/clarin/cmdi/vlo-exposure-frontend/${VLO_VERSION}/vlo-exposure-frontend-${SNAPSHOT_ID}.war"
NAME="vlo-exposure-frontend-${VLO_VERSION}"
VLO_DISTR_FILE="webapp/vlo-exposure-frontend.war"
VLO_DISTR_DIR="webapp/vlo-exposure-frontend"
